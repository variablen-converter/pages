String.prototype.replaceAt = function(_index, _newValue) 
{
    return this.substring(0, _index) + _newValue + this.substring(_index + _newValue.length)
}

function rxinize(string)
{
    if(string.length <= 2)
    {
        return string;
    }
    let needed_chars = string.length / 4;
    if(needed_chars < 2)
    {
        needed_chars = 2;
    }
    const vocals = ["a", "e", "i", "o", "u"];
    for(let i = 0; i < vocals.length; i++)
    {
        string = string.replaceAll(vocals[i], "");
    }
    if(string.length > needed_chars)
    {
        string = string.substring(0, needed_chars)
    }
    return string;
}

function vb_to_rx(string)
{
    let final_words = string.split(/(?=[A-Z])/);

    for(let i = 0; i < final_words.length; i++)
    {
        final_words[i] = rxinize(final_words[i]);
    }
    string = final_words.toString();
    string = string.replaceAll(',', '');
    
    return string;
}